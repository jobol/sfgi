(import scheme (chicken base) (chicken tcp) (r7rs))
(define (debug . x) (for-each display x))

;=============================================
;=============================================
;====                                    =====
;==== JUMPER                             =====
;====                                    =====
;=============================================
;=============================================

; Creates a jumper context and return its
; 2 components: the setter and the jumper
;
; (make-jumper) -> (values setjump longjump)
;
;   setjump := procedure thunk -> value passed to lumgjump
;   longjump := procedure any -> restore the continuation with the value
;
(define (make-jumper-set)
   ; waiter continuation or #f
   (define wait #f)
   ; call proc with continuation set
   (define (setjump proc . args)
      (call/cc (lambda (cont)
         (set! wait cont)
         (apply proc args))))
   ; jump to the set continuation
   (define (longjump value)
      (let ((func wait))
         (when func
            (set! wait #f)
            (func value))))
   ; result
   (values setjump longjump))

(define (make-jumper-param)
   ; waiter continuation or #f
   (define wait (make-parameter #f))
   ; call proc with continuation set
   (define (setjump proc . args)
      (call/cc (lambda (cont)
         (parameterize ((wait cont))
            (apply proc args)))))
   ; jump to the set continuation
   (define (longjump . args)
      (let ((func (wait)))
         (when func
            (apply func args))))
   ; result
   (values setjump longjump))

; pipe stream
(define (pipe-stream jump-maker)
   (define item #f)
   (define set? #f)
   (define-values (setjump longjump) (jump-maker))
   (define (setup onstarve)
      (define (get)
         (let loop ()
            (if set?
               (let ((value item))
                  (set! set? #f)
                  item)
               (begin
                  (setjump onstarve)
                  (loop)))))
      get)
   (define (put value)
      (set! item value)
      (set! set? #t)
      (longjump #f))
   (values setup put))

(define (stream-counter init end)
   (define current init)
   (lambda ()
      (if (> current end)
         (eof-object)
         (let ((value current))
            (set! current (+ value 1))
            value))))

; STREAM ---> PIPE --->
;         ^----'
(define (test-piped-stream stream jump-maker)
   (define-values (setup put) (pipe-stream jump-maker))
   (define (onstarve) (put (stream)))
   (define get (setup onstarve))
   get)

; STREAM ---> PIPE1 ---> PIPE2 --->
;         ^----'     ^----'
(define (test-double-piped-stream stream jump-maker)
   (test-piped-stream (test-piped-stream  stream jump-maker) jump-maker))

(define (process stream)
   (let loop ()
      (let ((value (stream)))
         (unless (eof-object? value)
            (display value)
            (newline)
            (loop)))))

(display "\ndirect\n")
(process (stream-counter 1 5))

(display "\npiped set\n")
(process (test-piped-stream (stream-counter 1 5) make-jumper-set))

(display "\npiped param\n")
(process (test-piped-stream (stream-counter 1 5) make-jumper-param))

(display "\ndouble-piped set\n")
(process (test-double-piped-stream (stream-counter 1 5) make-jumper-set))

(display "\ndouble-piped param\n")
(process (test-double-piped-stream (stream-counter 1 5) make-jumper-param))


(define (test stream jump-maker)
         (let-values (((setjump longjump) (jump-maker))
                      ((setup   put)      (pipe-stream jump-maker)))
            (let* ((onstarve (lambda() (longjump #t)))
                   (get      (setup onstarve)))
               (let loop ()
                  (let ((value (stream)))
                     (unless (eof-object? value)
                        (setjump (lambda()
                           (put value)
                           (process get)))
                        (loop)))))))


(display "\ntest set\n")
(test (stream-counter 1 5) make-jumper-set)

(display "\ntest param\n")
(test (stream-counter 1 5) make-jumper-param)



; vim: noai ts=3 sw=3 expandtab


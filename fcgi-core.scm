;=============================================
; FCGI is a Scheme library for making FAST CGI
; service in scheme.
;
; (c) FCGI authors, 2023
; SPDX-License-Identifier: 0BSD
;=============================================
; This file, fcgi-core.scm, defines all the
; basis features and is intended to be included
; in higher level modules offering higher
; level API and adapted to specific scheme
; implementations.
;=============================================
; This file has the following sections:
;  - FAST CGI DEFINITIONS
;  - BUFFERED WRITER
;  - BUFFERED READER
;  - JUMPER
;  - READ AND WRITE KEY/VALUE FOR PARAM
;  - RECORDS
;  - CHANNELS
;  - FCGI
;  - REQUESTS
;=============================================

(import (scheme case-lambda))

(define (pass . x) #t)
(define (debug . x) (for-each display x))

;=============================================
;=============================================
;====                                    =====
;==== FAST CGI DEFINITIONS               =====
;====                                    =====
;=============================================
;=============================================
; The definitions below are derived from
; FAST CGI specifications
;=============================================

; Listening socket file number
(define FCGI-LISTENSOCK-FILENO   0)

; Number of bytes in a FCGI-Header.
(define FCGI-HEADER-LEN          8)

; Value for version component of FCGI-Header
(define FCGI-VERSION-1           1)

; Values for type component of FCGI-Header
(define FCGI-BEGIN-REQUEST       1)
(define FCGI-ABORT-REQUEST       2)
(define FCGI-END-REQUEST         3)
(define FCGI-PARAMS              4)
(define FCGI-STDIN               5)
(define FCGI-STDOUT              6)
(define FCGI-STDERR              7)
(define FCGI-DATA                8)
(define FCGI-GET-VALUES          9)
(define FCGI-GET-VALUES-RESULT  10)
(define FCGI-UNKNOWN-TYPE       11)
(define FCGI-MAXTYPE            FCGI-UNKNOWN-TYPE)

; Value for requestId component of FCGI-Header
(define FCGI-NULL-REQUEST-ID     0)

; Mask for flags component of FCGI-BeginRequestBody
(define FCGI-KEEP-CONN           1)

; Values for role component of FCGI-BeginRequestBody
(define FCGI-RESPONDER           1)
(define FCGI-AUTHORIZER          2)
(define FCGI-FILTER              3)

; Values for protocolStatus component of FCGI-EndRequestBody
(define FCGI-REQUEST-COMPLETE    0)
(define FCGI-CANT-MPX-CONN       1)
(define FCGI-OVERLOADED          2)
(define FCGI-UNKNOWN-ROLE        3)

; Variable names for FCGI-GET-VALUES / FCGI-GET-VALUES-RESULT records
(define FCGI-MAX-CONNS  "FCGI-MAX-CONNS")
(define FCGI-MAX-REQS   "FCGI-MAX-REQS")
(define FCGI-MPXS-CONNS "FCGI-MPXS-CONNS")

;=============================================
;=============================================
;====                                    =====
;==== BUFFERED WRITER: WBUF              =====
;====                                    =====
;=============================================
;=============================================
;
; buffered writers are containing 4 items:
;
;   . a bytevector
;   . an offset number
;   . the procedure for really writing
;   . an optional procedure reporting close
;
; as an invariant, it is assumed that the offset
; if strictly less than the length of the byte vector 

(define-record-type <WBUF>
   (!wbuf! buffer offset proc close)
   wbuf?
   (buffer %wbuf-buffer %wbuf-buffer-set!)
   (offset %wbuf-offset %wbuf-offset-set!)
   (proc   %wbuf-proc)
   (close  %wbuf-close))

; create buffered writer for the write procedure
; 'write-proc' and a buffering 'size'
;
; (make-wbuf size write-proc close-proc) -> WBUF
;
;   size       := fixnum
;   write-proc := procedure bytevector -> void
;   close-proc := false | procedure -> void
;
(define (make-wbuf size write-proc close-proc)
   (!wbuf! (make-bytevector size) 0 write-proc close-proc))

; check if the write buffer is closed
;
; (wbuf-closed? wbuf) -> bool
;
;   wbuf := WBUF
;
(define (wbuf-closed? wbuf)
   (not (%wbuf-buffer wbuf)))

; flush the content of the write buffer
;
; (wbuf-flush wbuf) -> void
;
;   wbuf := WBUF
;
(define (wbuf-flush wbuf)
   (let* ((off (%wbuf-offset wbuf))
          (buf (%wbuf-buffer wbuf)))
      (unless (zero? off)
         ((%wbuf-proc wbuf) (bytevector-copy buf 0 off))
         (%wbuf-offset-set! wbuf 0))))

; close the write buffer
;
; (wbuf-close wbuf) -> void
;
;   wbuf := WBUF
;
(define (wbuf-close wbuf)
   (unless (wbuf-closed? wbuf)
      (wbuf-flush wbuf)
      (%wbuf-buffer-set! wbuf #f)
      (when (%wbuf-close wbuf)
         ((%wbuf-close wbuf)))))

; write data to the write buffer
;
; (wbuf-write wbuf item ...) -> void
;
;   wbuf := WBUF
;   item := vector | list | string | number | symbol | char | bytevector
;
(define (wbuf-write wbuf item . rest)
   (unless (wbuf-closed? wbuf)
      (cond
         ; recursively process complex types
         ((list?   item)
            (for-each (lambda (elem) (wbuf-write wbuf elem)) item))
         ((vector? item)
            (vector-for-each (lambda (elem) (wbuf-write wbuf elem)) item))
         ((string? item)
            (wbuf-write wbuf (string->utf8 item)))
         ((number? item)
            (wbuf-write wbuf (string->utf8 (number->string item))))
         ((symbol? item)
            (wbuf-write wbuf (string->utf8 (symbol->string item))))
         ((char?   item)
            (wbuf-write wbuf (string->utf8 (string item))))
         ; process bytevectors
         ((bytevector? item)
            (let* ((wsz (bytevector-length item)) ; size to write
                   (buf (%wbuf-buffer wbuf))      ; buffer
                   (off (%wbuf-offset wbuf))      ; offset in buffer
                   (bsz (bytevector-length buf))  ; size of buffer
                   (rsz (- bsz off)))             ; remaining size in buffer
               (if (> rsz wsz)
                  (begin
                        ; add data to the buffer without filling it
                        (unless (zero? wsz)
                           (bytevector-copy! buf off item 0 wsz))
                        (%wbuf-offset-set! wbuf (+ off wsz)))
                  (begin
                        ; fill the buffer and send it
                        (bytevector-copy! buf off item 0 rsz)
                        ((%wbuf-proc wbuf) (bytevector-copy buf))
                        ; write what remains
                        (let ((nwsz (- wsz rsz))) ; new size to be written
                           (if (< nwsz bsz)
                              (begin
                                    ; put it in the buffer because it can not fill the buffer
                                    (unless (zero? nwsz)
                                       (bytevector-copy! buf 0 item rsz wsz))
                                    (%wbuf-offset-set! wbuf nwsz))
                              (begin
                                    ; bigger than the buffer then send it directly
                                    ((%wbuf-proc wbuf) (bytevector-copy item rsz wsz))
                                    (%wbuf-offset-set! wbuf 0))))))))
         (else (pass))) ; error?
      (unless (null? rest)
         (wbuf-write wbuf rest))))

; write binary data to the write buffer
;
; (wbuf-write wbuf item ...) -> void
;
;   wbuf := WBUF
;   item := vector | list | string | number | symbol | char | bytevector
;
(define (wbuf-u8-write wbuf item . rest)
   (unless (wbuf-closed? wbuf)
      (cond
         ; recursively process complex types
         ((bytevector? item)
            (wbuf-write wbuf item))
         ((list?   item)
            (for-each (lambda (elem) (wbuf-u8-write wbuf elem)) item))
         ((vector? item)
            (vector-for-each (lambda (elem) (wbuf-u8-write wbuf elem)) item))
         ((and (exact-integer? item) (<= 0 item 255))
            ; a byte, add it to the buffer
            (let* ((off (%wbuf-offset wbuf))      ; offset in buffer
                   (nxt (+ off 1))                ; next offset
                   (buf (%wbuf-buffer wbuf))      ; buffer
                   (bsz (bytevector-length buf))) ; size of buffer
               ; add the byte to the buffer
               (bytevector-u8-set! buf off item)
               (if (< nxt bsz)
                  ; buffer not filled
                  (%wbuf-offset-set! wbuf nxt)
                  ; buffer filled, send it and reset
                  (begin
                     ((%wbuf-proc wbuf) (bytevector-copy buf))
                     (%wbuf-offset-set! wbuf 0)))))
         (else
            (wbuf-write wbuf item)))
      (unless (null? rest)
         (wbuf-u8-write wbuf rest))))

;=============================================
;=============================================
;====                                    =====
;==== BUFFERED READER                    =====
;====                                    =====
;=============================================
;=============================================
;
; buffered readers are containing 4 items:
;
;   . head of pending list (pair or empty list)
;   . tail of pending list (pair or #false)
;   . an offset number
;   . a procedure to call on starvation
;
; the procedure to be called on starvation should
; return: void -> void

(define-record-type <RBUF>
   (!rbuf! head tail offset starve)
   rbuf?
   (head   %rbuf-head   %rbuf-head-set!)
   (tail   %rbuf-tail   %rbuf-tail-set!)
   (offset %rbuf-offset %rbuf-offset-set!)
   (starve %rbuf-starve rbuf-starve-set!))

; create buffered read for the starvation procedure
; 'starve-proc'
;
; (make-rbuf starve-proc) -> RBUF
;
;   starve-proc := procedure void -> void
;
(define (make-rbuf starve-proc)
   (!rbuf! '() #f 0 starve-proc))

; check if the read buffer is closed
;
; (rbuf-closed? rbuf) -> bool
;
;   rbuf := RBUF
;
(define (rbuf-closed? rbuf)
   (and (not (%rbuf-head rbuf))
        (not (%rbuf-starve rbuf))))

; close the read buffer
;
; (rbuf-close rbuf) -> void
;
;   rbuf := RBUF
;
(define (rbuf-close rbuf)
   (rbuf-starve-set! rbuf #f)
   (unless (pair? (%rbuf-head rbuf))
      (%rbuf-head-set! rbuf #f)))

; add buffer to the buffered reader
;
; (rbuf-add rbuf item ...) -> void
;
;   rbuf := RBUF
;   item := vector | list | string | number | symbol | char | bytevector
;
(define (rbuf-add rbuf item . rest)
   (cond
      ; recursively process complex types
      ((list?   item)
         (for-each (lambda (elem) (rbuf-add rbuf elem)) item))
      ((vector? item)
         (vector-for-each (lambda (elem) (rbuf-add rbuf elem)) item))
      ((string? item)
         (rbuf-add rbuf (string->utf8 item)))
      ((number? item)
         (rbuf-add rbuf (string->utf8 (number->string item))))
      ((symbol? item)
         (rbuf-add rbuf (string->utf8 (symbol->string item))))
      ((char?   item)
         (rbuf-add rbuf (string->utf8 (string item))))
      ; process bytevectors
      ((bytevector? item)
         (unless (zero? (bytevector-length item))
            ; append the buffer to the pending list
            (let ((curtail (%rbuf-tail rbuf))
                  (nxttail (cons item '())))
               (if curtail
                  (set-cdr! curtail nxttail)
                  (%rbuf-head-set! rbuf nxttail))
               (%rbuf-tail-set! rbuf nxttail))))
      (else (pass))) ; error?
   (unless (null? rest)
      (rbuf-add rbuf rest)))

; add binary data to the add buffer
;
; (rbuf-add rbuf item ...) -> void
;
;   rbuf := RBUF
;   item := vector | list | string | number | symbol | char | bytevector
;
(define (rbuf-u8-add rbuf item . rest)
   (cond
      ; recursively process complex types
      ((bytevector? item)
         (rbuf-add rbuf item))
      ((list?   item)
         (apply rbuf-u8-add rbuf item))
      ((vector? item)
         (vector-for-each (lambda (elem) (rbuf-u8-add rbuf elem)) item))
      ((and (exact-integer? item) (<= 0 item 255))
         ; try to find le longest list of bytes
         (let ((lu8  (cons item rest)))
            (let loop ((iter lu8))
               (let ((nxt (cdr iter)))
                  (if (null? nxt)
                     (set! rest '())
                     (let ((val (car nxt)))
                        (if (and (exact-integer? val) (<= 0 val 255))
                           (loop nxt)
                           (begin
                              (set! rest nxt)
                              (set-cdr! iter '())))))))
            ; send thre bytevector
            (rbuf-add rbuf (apply bytevector lu8))))
      (else
         (rbuf-add rbuf item)))
   (unless (null? rest)
      (rbuf-u8-add rbuf rest)))

; returns the currently available length
;
; (rbuf-available-length rbuf) -> fixnum
;
;   rbuf := RBUF
;
(define (rbuf-available-length rbuf)
   (do ((iter (%rbuf-head rbuf)       (cdr iter))
        (resu (- (%rbuf-offset rbuf)) (+ resu (bytevector-length (car iter)))))
      ((null? iter) resu)))

; read data from the read buffer
;
; (rbuf-read rbuf) -> bytevector | eof
; (rbuf-read rbuf count) -> bytevector | eof
; (rbuf-read rbuf buffer [start [end]]) -> bytevector | eof
;
;   rbuf := RBUF
;   count := fixnum
;   buffer := bytevector
;   start := fixnum
;   end := fixnum
;
(define rbuf-read
   (let ()
      ; procedure for setting pending buffers
      (define (setbufs! rbuf lst)
         ; set head of buffers
         (%rbuf-head-set! rbuf lst)
         ; reset the offset
         (%rbuf-offset-set! rbuf 0)
         ; reset the tail if empty
         (unless (pair? lst)
            (%rbuf-tail-set! rbuf #f)
            ; manage eof
            (unless (%rbuf-starve rbuf)
               (%rbuf-head-set! rbuf #f))))

      ; procedure returning the first buffer
      (define (read-first rbuf)
         (let ((buf0 (%rbuf-head rbuf)))
            (cond
               ((pair? buf0)
                  (let ((bv  (car buf0))
                        (off (%rbuf-offset rbuf)))
                     ; move to next
                     (setbufs! rbuf (cdr buf0))
                     ; return the head
                     (if (zero? off)
                        bv
                        (bytevector-copy bv off))))
               ((null? buf0)
                  ; empty, claim starvation and loop
                  ((%rbuf-starve rbuf))
                  (read-first rbuf))
               (else
                  ; end of file found
                  (eof-object)))))

      ; procedure reading in tobuf
      (define (read-in rbuf tobuf start end pos)
         (if (= pos end)
            tobuf
            (let ((buf0 (%rbuf-head rbuf)))
               (cond
                  ((pair? buf0)
                     (let* ((bv  (car buf0))
                            (off (%rbuf-offset rbuf))
                            (len (bytevector-length bv))
                            (asz (- len off))
                            (rsz (- end pos)))
                        (if (< rsz asz)
                           (let ((nxtoff (+ off rsz)))
                              (bytevector-copy! tobuf pos bv off nxtoff)
                              (%rbuf-offset-set! rbuf nxtoff)
                              tobuf)
                           (let ((nxtpos (+ pos asz)))
                              (bytevector-copy! tobuf pos bv off len)
                              (setbufs! rbuf (cdr buf0))
                              (read-in rbuf tobuf start end nxtpos)))))
                  ((null? buf0)
                     ((%rbuf-starve rbuf))
                     (read-in rbuf tobuf start end pos))
                  (else
                     (if (= pos start)
                        (eof-object)
                        (do ((pos pos (+ pos 1)))
                           ((= pos end) tobuf)
                           (bytevector-u8-set! tobuf pos 0))))))))

      ; implementation of rbuf-read as case-lambda
      (case-lambda
         ((rbuf)
            (read-first rbuf))
         ((rbuf arg)
            (if (bytevector? arg)
               (read-in rbuf arg 0 (bytevector-length arg) 0)
               (read-in rbuf (make-bytevector arg) 0 arg 0)))
         ((rbuf tobuf start)
            (read-in rbuf tobuf start (bytevector-length tobuf) start))
         ((rbuf tobuf start end)
            (read-in rbuf tobuf start end start)))))

;=============================================
;=============================================
;====                                    =====
;==== JUMPER                             =====
;====                                    =====
;=============================================
;=============================================

; Creates a jumper context and return its
; 2 components: the setter and the jumper
;
; (make-jumper) -> (values setjump longjump)
;
;   setjump := procedure thunk -> value passed to lumgjump
;   longjump := procedure any -> restore the continuation with the value
;
(define (make-jumper)
   ; waiter continuation or #f
   (define wait #f)
   ; call proc with continuation set
   (define (setjump proc . args)
      (call/cc (lambda (cont)
         (set! wait cont)
         (apply proc args))))
   ; jump to the set continuation
   (define (longjump value)
      (let ((func wait))
         (when func
            (set! wait #f)
            (func value))))
   ; result
   (values setjump longjump))

;=============================================
;=============================================
;====                                    =====
;==== READ AND WRITE KEY/VALUE FOR PARAM =====
;====                                    =====
;=============================================
;=============================================
;
; write the alist as parameter list and then closes
; the write buffer
;
; (wbuf-write-params wbuf alist) -> void
;
;   wbuf := WBUF
;   alist := an a-list ((key . value) ...)
;
(define (wbuf-write-params wbuf alist)
   ; converts any value to a bytevector (or just try)
   (define (to-bytevector item)
      (cond
         ((string? item) (string->utf8 item))
         ((symbol? item) (string->utf8 (symbol->string item)))
         ((number? item) (string->utf8 (number->string item)))
         ((char?   item) (string->utf8 (string item)))
         ((list?   item) (apply bytevector-append (map to-bytevector item)))
         ((vector? item) (to-bytevector (vector->list item)))
         (else           item)))
   ; write a length
   (define (write-length len)
      (if (< len 128)
         ; single byte length
         (wbuf-u8-write wbuf len)
         ; 4 bytes length
         (let*-values (((l321 l0) (floor/ len 256))
                       ((l32  l1)  (floor/ l321 256))
                       ((l3   l2)  (floor/ l32 256)))
            (wbuf-u8-write wbuf (list (+ l3 128) l2 l1 l0)))))
   ; write a pair key / value
   (define (write-pair pair)
      (let* ((key  (to-bytevector (car pair)))
             (val  (to-bytevector (cdr pair)))
             (lkey (bytevector-length key))
             (lval (bytevector-length val)))
         (write-length lkey)
         (write-length lval)
         (wbuf-write wbuf key)
         (wbuf-write wbuf val)))
   ; write all key / value pairs
   (for-each write-pair alist)
   (wbuf-close wbuf))

; read the parameters from a read buffer until its end
; and returns an alist
;
; (wbuf-read-params rbuf) -> alist
;
;   rbuf := RBUF
;   alist := an a-list ((key . value) ...)
;
(define (rbuf-read-params rbuf)
   ; for reading lengths
   (define buf #u8(0 0 0 0))
   ; read a length
   (define (read-length)
      (if (eof-object? (rbuf-read rbuf buf 0 1))
         ; end found
         (eof-object)
         (let ((len (bytevector-u8-ref buf 0)))
            (if (< len 128)
               ; one byte length
               len
               ; four bytes length, read 3 remaining bytes
               (if (eof-object? (rbuf-read rbuf buf 1 4))
                  ; premature end found
                  (eof-object)
                  ; compute the length
                  (+ (* 16777216 (- len 128))
                     (* 65536 (bytevector-u8-ref buf 1))
                     (* 256 (bytevector-u8-ref buf 2))
                     (bytevector-u8-ref buf 3)))))))
   ; read the key / values until the end of the input
   (let loop ((resu '()))
      (let ((lkey (read-length))
            (lval (read-length)))
         (if (eof-object? lval)
            resu
            (let ((key  (rbuf-read rbuf lkey))
                  (val  (rbuf-read rbuf lval)))
               (if (eof-object? val)
                  resu
                  (let ((key (string->symbol (utf8->string key)))
                        (val (utf8->string val)))
                     (loop (cons (cons key val) resu)))))))))

(define (params-bytevector->alist bv)
   (let ((rbuf   (make-rbuf #f)))
      (rbuf-add rbuf content)
      (rbuf-close rbuf)
      (rbuf-read-params rbuf)))

(define (params-alist->bytevector alist)
   (let* ((rbuf   (make-rbuf #f))
          (wbuf   (make-wbuf 200
                             (lambda x (apply rbuf-add rbuf x))
                             (lambda () (rbuf-close rbuf)))))
      (wbuf-write-params wbuf alist)
      (let* ((len (rbuf-available-length rbuf))
             (bv  (make-bytevector len)))
         (rbuf-read bv 0 len))))

;=============================================
;=============================================
;====                                    =====
;==== RECORDS                            =====
;====                                    =====
;=============================================
;=============================================
;
; data structure for FCGI records
;
; Defines a fcgi record
(define-record-type <RECORD>
   (!record! channel version type id content)
   record?
   (channel  record-channel)  ; the channel of the record
   (version  record-version)  ; version of the protocol
   (type     record-type)     ; type of record
   (id       record-id)       ; the request id 
   (content  record-content)) ; a bytevector for the content

;=============================================
;=============================================
;====                                    =====
;==== CHANNELS                           =====
;====                                    =====
;=============================================
;=============================================
;
; a communication channel is the channel received
; after accepting an incomming connexion. It hold
; multiplexed messages of the Fast CGI protocol.
; But at the level of channels, it does not care.
;
; channels are intended in the core to abstract
; the real implementation.
;
; channels are containing the following items:
;
;  . the FCGI it belongs to
;  . a procedure for receiving data: buffer [start [end]] -> bytevector | eof
;  . a procedure for sending data: bytevector -> void
;  . a procedure for closing the channel: void -> void
;  . a close status
;  . a procedure to be called when starved some where, that procedure takes
;                no argument and either return or not but better not.
;
; Defines an fcgi channel
(define-record-type <CHANNEL>
   (!channel! fcgi receive send close closed? starve)
   channel?
   (fcgi      channel-fcgi)      ; the FCGI for that channel
   (receive   %channel-receive)  ; read procedure: bytevector start end -> bytevector
   (send      %channel-send)     ; write procedure: bytevector ->
   (close     %channel-close)    ; close the channel
   (closed?   channel-closed? channel-closed-set?) ; handle close state
   (starve    channel-starve channel-starve-set!))   ; the procedure to call 

; primitive for receiving a bytevector from a channel
;
; (channel-receive channel [count]) -> bytevector | eof
; (channel-receive channel buffer [start [end]]) -> bytevector | eof
;
;   channel := <CHANNEL>
;   count := fixnum
;   buffer := bytevector
;   start := fixnum
;   end := fixnum
;
(define (channel-receive channel . args)
   (if (channel-closed? channel)
      (eof-object)
      (apply (%channel-receive channel) args)))

; primitive for sending a bytevector
;
; (channel-send channel buffer [start [end]]) -> void
;
;   channel := <CHANNEL>
;   buffer := bytevector
;   start := fixnum
;   end := fixnum
;
(define (channel-send channel bv)
   (unless (channel-closed? channel)
      ((%channel-send channel) bv)))

; primitive for closing the channel
;
; (channel-close channel) -> void
;
;   channel := <CHANNEL>
;
(define (channel-close channel)
   (unless (channel-closed? channel)
      (channel-closed-set? channel #t)
      ((%channel-close channel))))

; send a record
;
; (channel-send-record channel type id content) -> void
;
;   channel := <CHANNEL>
;   type := fixnum, 0..255, the type of the request
;   id := fixnum, 0..65535, the request id
;   content := bytevector, the content to send
;
(define (channel-send-record channel type id content)
   (let*-values (((id1 id0) (floor/ id 256))
                 ((cl1 cl0) (floor/ (bytevector-length content) 256))
                 ((pl)      (remainder (- 256 cl0) 8))
                 ((head)    (bytevector FCGI-VERSION-1 type id1 id0 cl1 cl0 pl 0)))
      (channel-send channel head)
      (channel-send channel content)
      (unless (zero? pl)
         (let ((pad  (make-bytevector pl 0)))
            (channel-send channel pad)))))

; send a record whose content is 8 bytes long
;
; (channel-send-8 channel type id b0 b1 b2 b3 b4 b5 b6 b7) -> void
;
;   channel := <CHANNEL>
;   type := fixnum, 0..255, the type of the request
;   id := fixnum, 0..65535, the request id
;   b0..7 := fixnum, 0..255, the content to send
;
; send unknown-type record
(define (channel-send-8 channel type id b0 b1 b2 b3 b4 b5 b6 b7)
   (let*-values (((id1 id0) (floor/ id 256))
                 ((content) (bytevector FCGI-VERSION-1 type id1 id0 0 8 0 0
                                        b0 b1 b2 b3 b4 b5 b6 b7)))
      (channel-send channel content)))

; send unknown-type record
;
; (channel-send-unknown-type channel type) -> void
;
;   channel := <CHANNEL>
;   type := fixnum, 0..255, the unknown type
;
(define (channel-send-unknown-type channel type)
   (channel-send-8 channel FCGI-UNKNOWN-TYPE FCGI-NULL-REQUEST-ID
      type 0 0 0 0 0 0 0))

; send unknown role request end
;
; (channel-send-unknown-role channel id) -> void
;
;   channel := <CHANNEL>
;   id := fixnum, 0..255, the request id for the unknown role
;
(define (channel-send-unknown-role channel id)
   (channel-send-8 channel FCGI-END-REQUEST id
      0 0 0 0 FCGI-UNKNOWN-ROLE 0 0 0))

; receive a record from channel
;
; (channel-receive-record channel) -> RECORD | eof
;
;   channel := <CHANNEL>
;
(define (channel-receive-record channel)
   (let* ((head  (make-bytevector FCGI-HEADER-LEN))
          (rcv   (channel-receive channel head 0 FCGI-HEADER-LEN)))
      (if (eof-object? rcv)
         rcv
         (let* ((version  (bytevector-u8-ref head 0))
                (type     (bytevector-u8-ref head 1))
                (id       (+ (* (bytevector-u8-ref head 2) 256)
                             (bytevector-u8-ref head 3)))
                (c-len    (+ (* (bytevector-u8-ref head 4) 256)
                             (bytevector-u8-ref head 5)))
                (p-len    (bytevector-u8-ref head 6))
                (content  (make-bytevector c-len))
                (rcv      (if (zero? c-len) #t (channel-receive channel content 0 c-len))))
            (if (eof-object? rcv)
               rcv
               (begin
                  (unless (zero? p-len)
                     (let ((scratch (if (<= p-len FCGI-HEADER-LEN)
                                       head (make-bytevector p-len))))
                        (channel-receive channel scratch 0 p-len)))
                  (!record! channel version type id content)))))))

;=============================================
;=============================================
;====                                    =====
;==== FCGI                               =====
;====                                    =====
;=============================================
;=============================================
;
; Defines an fcgi channel
(define-record-type <FCGI>
   (!fcgi! handler max-reqs max-conns requests)
   fcgi?
   (handler   fcgi-handler)
   (max-reqs  fcgi-max-reqs)
   (max-conns fcgi-max-conns)
   (requests  fcgi-requests))

; creates an fcgi instance
;
; (make-fcgi handler [max-reqs [max-conns]]) -> FCGI
;
;   handler := procedure for handling requests: REQUEST -> void
;   max-reqs := fixnum, defaults to 1
;   max-conns := fixnum, defaults to 1
;
(define (make-fcgi handler . others)
   (define max-reqs 1)
   (define max-conns 1)
   ; check optional arguments
   (unless (null? others)
      (set! max-reqs (car others))
      (unless (null? (cdr others))
         (set! max-conns (cadr others))))
   ; create the vector of requests
   (let ((requests (make-vector (+ 1 max-reqs) #f)))
      ; creates the FCGI and return it
      (!fcgi! handler max-reqs max-conns requests)))

; get the request at index
(define (fcgi-request fcgi id)
   (vector-ref (fcgi-requests fcgi) id))

; set the request at index
(define (fcgi-request-set! fcgi id value)
   (vector-set! (fcgi-requests fcgi) id value))

; compose the result to the query FCGI-GET-VALUES
; the query is an alist of the required values
(define (fcgi-get-values fcgi query)
   ; filter the returned values accordingly to the request
   (define (filter resu knowns)
      (if (null? knowns)
         resu
         (let* ((item     (car knowns))
                (queried? (assoc (car item) query)))
            (filter (if queried? (cons item resu) resu) (cdr knowns)))))
   ; the current possibly returned values
   (filter '() `((,FCGI-MAX-CONNS  . ,(number->string (fcgi-max-conns fcgi)))
                 (,FCGI-MPXS-CONNS . "1")
                 (,FCGI-MAX-REQS   . ,(number->string (fcgi-max-reqs fcgi))))))

;=============================================
;=============================================
;====                                    =====
;==== REQUESTS                           =====
;====                                    =====
;=============================================
;=============================================
;
;

; defines a fcgi request
(define-record-type <REQUEST>
   (!request! channel id role flags
              outbuf errbuf
              inbuf databuf paramsbuf
              inadd dataadd paramsadd
              alist)
   request?
   (channel     request-channel %request-channel-set!)
   (id          request-id)
   (role        request-role)
   (flags       request-flags)
   (outbuf      request-outbuf)
   (errbuf      request-errbuf)
   (inbuf       request-inbuf)
   (databuf     request-databuf)
   (paramsbuf   %request-paramsbuf)
   (inadd       %request-inadd)
   (dataadd     %request-dataadd)
   (paramsadd   %request-paramsadd)
   (alist       %request-alist %request-alist-set!))

; check if the request is closed or not
;
; (request-closed? request) -> boolean
;
;    request := REQUEST
;
(define (request-closed? request)
   (not (request-channel request)))

; terminate the request
(define (request-close request)
   (let ((channel (request-channel request)))
      (when channel
         (wbuf-close (request-outbuf request))
         (wbuf-close (request-errbuf request))
         (if (zero? (request-flags request))
            (channel-close channel))
         (fcgi-request-set! (channel-fcgi channel) (request-id request) #f)
         (%request-channel-set! request #f))))

; send for request a record of given type
;
; (%request-send-record request type content) -> void
;
;    request := REQUEST
;    type := fixnum, 0..255, type of record
;    content := bytevector
;
(define (%request-send-record request type content)
   (let ((channel (request-channel request)))
      (when channel
         (channel-send-record channel type (request-id request) content))))

; send for request a record whose content is 8 bytes long
;
; (record-send-8 request type b0 b1 b2 b3 b4 b5 b6 b7) -> void
;
;   request := REQUEST
;   type := fixnum, 0..255, the type of the request
;   b0..7 := fixnum, 0..255, the content to send
;
(define (%request-send-8 request type b0 b1 b2 b3 b4 b5 b6 b7)
   (let ((channel (request-channel request)))
      (when channel
         (channel-send-8 channel type (request-id request) b0 b1 b2 b3 b4 b5 b6 b7))))

; send end request
;
; (request-send-end request app-status proto-status) -> void
;
;    request := REQUEST
;    app-status := fixnum, application status
;    proto-status := fixnum, 0..255, protocol status
;
(define (%request-send-end request app-status proto-status)
   ; build the status content
   (unless (request-closed? request)
      (wbuf-close (request-outbuf request))
      (wbuf-close (request-errbuf request))
      (let*-values (((as321 as0)  (floor/ app-status 256))
                    ((as32  as1)  (floor/ as321 256))
                    ((as3   as2)  (floor/ as32 256)))
         (%request-send-8 request FCGI-END-REQUEST as3 as2 as1 as0 proto-status 0 0 0))
      (request-close request)))

(define (%request-make-wbuf channel id type)
   (make-wbuf
      4000
      (lambda (bv) (channel-send-record channel type id bv))
      (lambda ()   (channel-send-record channel type id #u8()))))

(define (%request-make-rbuf starve)
   (let-values (((setjump longjump) (make-jumper)))
      (let* ((rbuf   (make-rbuf (lambda () (setjump starve))))
             (add    (lambda (bv)
                        (let ((end? (zero? (bytevector-length bv))))
                           (if end?
                              (rbuf-close rbuf)
                              (rbuf-add rbuf bv))
                           (longjump (not end?))))))
         (values rbuf add))))

; creates a request
(define (make-request channel id role flags)
   (define outbuf (%request-make-wbuf channel id FCGI-STDOUT))
   (define errbuf (%request-make-wbuf channel id FCGI-STDERR))
   (define starve (channel-starve channel))
   (let-values (((paramsbuf paramsadd) (%request-make-rbuf starve))
                ((inbuf     inadd)     (%request-make-rbuf starve))
                ((databuf   dataadd)   (%request-make-rbuf starve)))
      (!request! channel id role flags
                 outbuf errbuf
                 inbuf databuf paramsbuf
                 inadd dataadd paramsadd
                 #f)))

(define (request-end-unknown-role request)
   (%request-send-end request 0 FCGI-UNKNOWN-ROLE))

(define request-end
   (case-lambda
      ((request)
            (%request-send-end request 0 FCGI-REQUEST-COMPLETE))
      ((request app-status)
            (%request-send-end request app-status FCGI-REQUEST-COMPLETE))
      ((request app-status proto-status)
            (%request-send-end request app-status proto-status))))

(define (request-params request)
   (let ((params (%request-alist request)))
      (if params
         params
         (let ((params (rbuf-read-params (%request-paramsbuf request))))
            (%request-alist-set! request params)
            params))))

(define (request-write-stdout request value)
   (wbuf-write (request-outbuf request) value))

(define (request-write-stderr request value)
   (wbuf-write (request-errbuf request) value))





;-------< receiving >------------

; handle unexpected FCGI record type
(define (on-unexpected record)
   (channel-send-unknown-type (record-channel record) (record-type record)))

; handle begin of request
(define (on-begin-request record)
   ; get id, role, channel, fcgi and role's handler
   (let* ((content  (record-content record))
          (r1       (bytevector-u8-ref content 0))
          (r0       (bytevector-u8-ref content 1))
          (role     (+ r0 (* 256 r1)))
          (id       (record-id record))
          (channel  (record-channel record))
          (fcgi     (channel-fcgi channel))
          (flags    (bytevector-u8-ref content 2))
          (request  (make-request channel id role flags)))
            (fcgi-request-set! fcgi id request)
            ((fcgi-handler fcgi) request)))

; handle abort of request
(define (on-abort-request request record)
   (pass)) ; TODO but spec says that it must be passed to application

; handle of FCGI-PARAMS
(define (on-params request record)
   (when request
      ((%request-paramsadd request) (record-content record))))

; handle of FCGI-STDIN
(define (on-stdin request record)
   (when request
      ((%request-inadd request) (record-content record))))

; handle of FCGI-DATA
(define (on-data request record)
   (when request
      ((%request-dataadd request) (record-content record))))

; handle begin of FCGI-GET-VALUES
(define (on-get-values record)
   (let* ((channel    (record-channel record))
          (fcgi       (channel-fcgi channel))
          (content    (record-content record))
          (query      (params-bytevector->alist content))
          (result     (fcgi-get-values fcgi query))
          (reply      (params-alist->bytevector result)))
         (channel-send-record channel FCGI-GET-VALUES-RESULT (record-id record) reply)))

; calls the procedure proc with the request of t
(define (with-request proc)
   (lambda (record)
      (let* ((channel  (record-channel record))
             (id       (record-id record))
             (fcgi     (channel-fcgi channel))
             (request  (fcgi-request fcgi id)))
         (proc request record))))

; default processing
(define on-type `(
   (,FCGI-BEGIN-REQUEST      . ,on-begin-request)
   (,FCGI-ABORT-REQUEST      . ,(with-request on-abort-request))
   (,FCGI-END-REQUEST        . ,on-unexpected)
   (,FCGI-PARAMS             . ,(with-request on-params))
   (,FCGI-STDIN              . ,(with-request on-stdin))
   (,FCGI-STDOUT             . ,on-unexpected)
   (,FCGI-STDERR             . ,on-unexpected)
   (,FCGI-DATA               . ,(with-request on-data))
   (,FCGI-GET-VALUES         . ,on-get-values)
   (,FCGI-GET-VALUES-RESULT  . ,on-unexpected)
   (,FCGI-UNKNOWN-TYPE       . ,on-unexpected)))


; receive a new message from fcgi and dispatch it
(define (fcgi-dispatch channel)
   ; receive the header
   (let ((record (channel-receive-record channel)))
      (if (eof-object? record)
         (channel-close channel)
         ; check FCGI version
         (if (= (record-version record) FCGI-VERSION-1)
            ; valid version get type's processor
            (let* ((item (assoc (record-type record) on-type))
                   (proc (if item (cdr item) on-unexpected)))
                  (proc record))
            ; invlid version
            (channel-close channel)))))
;=============================================
; vim: noai ts=3 sw=3 expandtab


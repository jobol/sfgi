
(define-library (fcgi)
   (export
      (rename request?             fastcgi-request?)
      (rename request-role         fastcgi-request-role)
      (rename request-params       fastcgi-request-params)
      (rename request-write-stdout fastcgi-write-stdout)
      (rename request-write-stderr fastcgi-write-stderr)
      (rename request-end          fastcgi-request-end)

(include fcgi-core.scm)

;-------< FROM GERBIL >-------------------
;
; (start-fastcgi-server! address respond) -> server | error
;
;    address := string; address and port to listen
;    respond := function to handle requests
;
; start multi-threaded server listening on address.
; Dispatches requests to respond procedure.
;
(define (start-fastcgi-server! address respond)
   )
; 
; (fastcgi-request? obj) -> boolean
;
;  obj := any object
;
; Return #t if obj is fastcgi object, #f otherwise.
;
;
; (fastcgi-request-role req) -> fixnum
;
;  req := FastCGI request object
;
; Return role from req. Role is a positive fixnum starting from 1:
;
; FCGI-RESPONDER  1
; FCGI-AUTHORIZER 2
; FCGI-FILTER     3
;
;
; (fastcgi-request-params req) -> list
;
;  req := FastCGI request object
;
; Return params list from req.
;
;
; (fastcgi-request-stdin req) -> port
;
;  req := FastCGI request object
;
; Return stdin from req.
;
(define (fastcgi-request-stdin req)
)
;
; (fastcgi-write-stdout req data) -> void | error
;
;  req  := FastCGI request object
;  data := string | u8vector
;
; Write data to req's output port. Signals an error if no port is set in req.
;
;
; (fastcgi-write-stderr req data) -> void | error
;
;  req  := FastCGI request object
;  data := string | u8vector
;
; Write data to req's error port. Signals an error if no port is set in req.
;
;
; (fastcgi-request-end req [app-status = 0] [proto-status = FCGI-REQUEST-COMPLETE]) -> void
;
;  req          := FastCGI request object
;  app-status   := fixnum;
;  proto-status := fixnum; fcgi protocol status, see below
;
; Marks FastCGI request as ended. The proto-status accepted values as as follows:
; FCGI-REQUEST-COMPLETE 0
; FCGI-CANT-MPX-CONN    1
; FCGI-OVERLOADED       2
; FCGI-UNKNOWN-ROLE     3
;
; vim: noai ts=3 sw=3 expandtab

; vim: noai ts=3 sw=3 expandtab

;(import scheme (chicken base) (r7rs))
;(import (r7rs))

;
(define (make-jumper)
   ; waiting continuation
   (let ((wait #f))
      (values
         ; call proc with continuation set
         (lambda (proc)
            (call/cc (lambda (cont)
               (set! wait cont)
               (proc)
               (set! wait #f))))
         ; jump to the set continuation
         (lambda ()
            (let ((func wait))
               (when func
                  (set! wait #f)
                  (func #f)))))))

(define (test)
   (let* ((iter (list 0 1 2 3 4 5 6 7 8 9))
          (cur  '()))
      (let-values (((setjmp longjmp) (make-jumper)))
         (define (put item)
            (set! cur item)
            (longjmp))
         (define (end)
            (set! cur #f)
            (longjmp))
         (define (transfer)
            (let ((head iter))
               (if (null? head)
                  (end)
                  (begin
                     (set! iter (cdr iter))
                     (put (car head))))))
         (define (get)
            (setjmp transfer)
            cur)
         (let loop ()
            (let ((obj (get)))
               (when obj
                  (display obj)
                  (newline)
                  (loop)))))))
      

(test)


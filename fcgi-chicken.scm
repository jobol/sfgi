; vim: noai ts=3 sw=3 expandtab


;(module fcgi
;   (run-fcgi)

(import scheme
   (chicken base)
   (chicken tcp)
   (r7rs))


(include "fcgi-core.scm")

; creates the channel for the given fcgi and the filedescriptor
(define (make-channel fcgi inport outport starve)
    (!channel! ; fcgi context receive send close
        fcgi
        (lambda (buffer start end)
           (read-bytevector! buffer inport start end))
        (lambda (buffer)
           (write-bytevector buffer outport))
        (lambda ()
           (close-port inport)
           (close-port outport))
        #f
        starve))

; run the fcgi server
(define (run-fcgi port handler)
   (let ((fcgi     (make-fcgi handler))
         (listener (tcp-listen port)))
      (let main-loop ()
         (let-values (((inport outport) (tcp-accept listener))
                      ((setjump longjump) (make-jumper)))
            (let ((channel (make-channel fcgi inport outport
                                 (lambda() (longjump #t)))))
               (let channel-loop ()
                  (unless (port-closed? inport)
                     (setjump fcgi-dispatch channel)
                     (channel-loop)))))
         (main-loop))))


(define (http-send-html put-out . data)
   (let* ((utf8 (string->utf8 (apply string-append data)))
          (len  (bytevector-length utf8))
          (crnl "\r\n")
          (type "content-type: text/html; charset=UTF-8")
          (clen (string-append "content-length: " (number->string len)))
;          (hth  crnl))
          (hth  (string-append type crnl clen crnl crnl)))
;          (hth  (string-append type crnl crnl)))
      (put-out (string->utf8 hth))
      (put-out utf8)))

(define count 0)
(define (my-responder request)
   (set! count (+ count 1))
   (http-send-html (lambda (bv) (wbuf-write (request-outbuf request) bv))
       "<html><body>here it is " (number->string count) "!\n")
   (request-end request 0))

(define (my-handler request)
   (display "ENTERING HANDLER\n")
   (display "params: ")
   (display (request-params request))
   (newline)
   (if (equal? FCGI-RESPONDER (request-role request))
      (my-responder request)
      (request-end-unknown-role request)))

(run-fcgi 9999 my-handler)

